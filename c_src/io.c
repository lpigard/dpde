#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "system.h"
#include "io.h"
#include "pcg.h"
#include "integrate.h"
#include "ana.h"
#include "mesh.h"
#include "sets.h"
#if defined(_OPENMP)
#include <omp.h>
#endif


volatile int NoKeyboardInterrupt = 1;

void intHandler(int dummy)
{
    printf("Catched Signal to Stop.\n");
    NoKeyboardInterrupt = 0;
}

int initialize(SYSTEM * s, int argc, char *argv[])
{
    signal(SIGTSTP, intHandler);
    if(argc - 1 != 4)
    {
        printf("USAGE: ./run prefix conf-file ana-file simulation-time (%d / 4)\n", argc - 1);
        return -1;
    }

    s->prefix = argv[1];
    s->conf_file_name = argv[2];
    s->ana_file_name = argv[3];
    s->sim_time_double = atof(argv[4]);

    read_conf_scalars(s);

    printf("########################################################################################################################\n");
    printf("Simulation parameters:\n");
    printf("n_beads %d n_cells %d n_polys %d bl_len_max %d time %lg lx %d ly %d lz %d dt %lg n_bead_types %d conf_save_dt_double %lg ana_dt_double %lg info_dt %lg walltime %d\n", s->n_beads, s->n_cells, s->n_polys, s->bl_len_max, s->time, s->lx, s->ly, s->lz, s->dt, s->n_bead_types, s->conf_save_dt_double, s->ana_dt_double, s->info_dt, s->walltime);

    s->sim_time_int = (int)nearbyint(s->sim_time_double / s->dt);
    s->conf_save_dt_int = (int)nearbyint(s->conf_save_dt_double / s->dt);
    s->ana_dt_int = (int)nearbyint(s->ana_dt_double / s->dt);

    int n_beads = s->n_beads;
    int n_bead_types = s->n_bead_types;
    int n_polys = s->n_polys;
    int n_cells = s->n_cells;
    s->cl_len_max = 0;

    s->rx = (double*)malloc(n_beads * sizeof(double));
    s->ry = (double*)malloc(n_beads * sizeof(double));
    s->rz = (double*)malloc(n_beads * sizeof(double));
    s->rx0 = (double*)malloc(n_beads * sizeof(double));
    s->ry0 = (double*)malloc(n_beads * sizeof(double));
    s->rz0 = (double*)malloc(n_beads * sizeof(double));
    s->px = (double*)malloc(n_beads * sizeof(double));
    s->py = (double*)malloc(n_beads * sizeof(double));
    s->pz = (double*)malloc(n_beads * sizeof(double));
    s->e = (double*)malloc(n_beads * sizeof(double));
    s->fx = (double*)malloc(n_beads * sizeof(double));
    s->fy = (double*)malloc(n_beads * sizeof(double));
    s->fz = (double*)malloc(n_beads * sizeof(double));
    s->bead_type = (int*)malloc(n_beads * sizeof(int));
    s->c = (double*)malloc(n_bead_types * sizeof(double));
    s->m_inv = (double*)malloc(n_bead_types * sizeof(double));
    s->gamma = (double*)malloc(n_bead_types * n_bead_types * sizeof(double));
    s->f_bonded = (double*)malloc(n_bead_types * n_bead_types * sizeof(double));
    s->f_non_bonded = (double*)malloc(n_bead_types * n_bead_types * sizeof(double));
    s->kappa_bonded = (double*)malloc(n_bead_types * n_bead_types * sizeof(double));
    s->kappa_non_bonded = (double*)malloc(n_bead_types * n_bead_types * sizeof(double));
    s->phi = (int*)malloc(n_bead_types * n_cells * sizeof(int));
    s->cl = (int*)malloc(n_cells * s->cl_len_max * sizeof(int));
    s->bl = (int*)malloc(n_beads * s->bl_len_max * sizeof(int));
    s->nl = (int*)malloc(n_beads * 14 * s->cl_len_max * sizeof(int));
    s->cl_len = (int*)malloc(n_cells * sizeof(int));
    s->bl_len = (int*)malloc(n_beads * sizeof(int));
    s->nl_len = (int*)malloc(n_beads * 14 * sizeof(int));
    s->cell_neighbor = (int*)malloc(n_cells * 14 * sizeof(int));
    s->poly_offset = (int*)malloc(n_polys * sizeof(int));
    s->poly_N = (int*)malloc(n_polys * sizeof(int));

    s->n_mc = 0;
    s->n_mc_bl = 0;
    s->n_mc_nl = 0;
    s->n_acc = 0;
    s->n_acc_bl = 0;
    s->n_acc_nl = 0;

    s->time_0 = s->time;

    printf("INFO: Take initial condition from configuration file %s.\n", s->conf_file_name);
    read_conf_arrays(s);

    calc_cell_neighbor(s);

    if(calc_bl_set(s) == -1)
    {
        return -1;
    }

    if(calc_nl_set(s) == -1)
    {
        return -1;
    }

    for(int i = 0; i < n_beads; i++)
    {
        s->fx[i] = 0.;
        s->fy[i] = 0.;
        s->fz[i] = 0.;

        s->rx0[i] = s->rx[i];
        s->ry0[i] = s->ry[i];
        s->rz0[i] = s->rz[i];
    }

    s->ana_file_id = H5Fopen(s->ana_file_name, H5F_ACC_RDWR, H5P_DEFAULT);

    #if defined(_OPENMP)
    #pragma omp parallel
    {
        printf("n_threads = %d\n", omp_get_num_threads());
    }
    #endif

    s->n_rngs = s->bl_set_len_max > s->nl_set_len_max ? s->bl_set_len_max : s->nl_set_len_max;

    // initialize random number generators
    s->pcgs_main = &s->pcgstate_main;
    seed_pcg32(s->pcgs_main, time(NULL), 1);
    s->pcgstate = (PCG_STATE*)malloc(s->n_rngs * sizeof(PCG_STATE));

    for(int i = 0; i < s->n_rngs; i++)
    {
        seed_pcg32(&s->pcgstate[i], time(NULL) + (i + 1) * 10, 1);
    }

    #pragma acc enter data copyin(s[0:1])
    #pragma acc enter data copyin(s->rx[0:s->n_beads])
    #pragma acc enter data copyin(s->ry[0:s->n_beads])
    #pragma acc enter data copyin(s->rz[0:s->n_beads])
    #pragma acc enter data copyin(s->rx0[0:s->n_beads])
    #pragma acc enter data copyin(s->ry0[0:s->n_beads])
    #pragma acc enter data copyin(s->rz0[0:s->n_beads])
    #pragma acc enter data copyin(s->px[0:s->n_beads])
    #pragma acc enter data copyin(s->py[0:s->n_beads])
    #pragma acc enter data copyin(s->pz[0:s->n_beads])
    #pragma acc enter data copyin(s->e[0:s->n_beads])
    #pragma acc enter data copyin(s->fx[0:s->n_beads])
    #pragma acc enter data copyin(s->fy[0:s->n_beads])
    #pragma acc enter data copyin(s->fz[0:s->n_beads])
    #pragma acc enter data copyin(s->bead_type[0:s->n_beads])
    #pragma acc enter data copyin(s->c[0:s->n_bead_types])
    #pragma acc enter data copyin(s->m_inv[0:s->n_bead_types])
    #pragma acc enter data copyin(s->gamma[0:s->n_bead_types*s->n_bead_types])
    #pragma acc enter data copyin(s->f_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc enter data copyin(s->f_non_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc enter data copyin(s->kappa_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc enter data copyin(s->kappa_non_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc enter data copyin(s->phi[0:s->n_bead_types*s->n_cells])
    #pragma acc enter data copyin(s->cl[0:s->n_cells*s->cl_len_max])
    #pragma acc enter data copyin(s->bl[0:s->n_beads*s->bl_len_max])
    #pragma acc enter data copyin(s->nl[0:s->n_beads*14*s->cl_len_max])
    #pragma acc enter data copyin(s->cl_len[0:s->n_cells])
    #pragma acc enter data copyin(s->bl_len[0:s->n_beads])
    #pragma acc enter data copyin(s->nl_len[0:s->n_beads*14])
    #pragma acc enter data copyin(s->cell_neighbor[0:s->n_cells*14])
    #pragma acc enter data copyin(s->poly_offset[0:s->n_polys])
    #pragma acc enter data copyin(s->poly_N[0:s->n_polys])
    #pragma acc enter data copyin(s->bl_set[0:s->bl_n_sets*s->bl_set_len_max])
    #pragma acc enter data copyin(s->bl_set_len[0:s->bl_n_sets])
    #pragma acc enter data copyin(s->nl_set[0:s->nl_n_sets*s->nl_set_len_max])
    #pragma acc enter data copyin(s->nl_set_len[0:s->nl_n_sets])
    #pragma acc enter data copyin(s->pcgstate[0:s->n_rngs])

    fill_cl(s);
    fill_nl(s);
    bonded_forces_set(s);
    non_bonded_forces_set(s);

    printf("INFO: Start simulation with %d polymers, %d monomers\n", n_polys, n_beads);
    return 0;
}

void finalize(SYSTEM * s)
{
    printf("Simulation done. Clean up.\n");
    #if 0
    #pragma acc exit data copyout(s->rx[0:s->n_beads])
    #pragma acc exit data copyout(s->ry[0:s->n_beads])
    #pragma acc exit data copyout(s->rz[0:s->n_beads])
    #pragma acc exit data copyout(s->rx0[0:s->n_beads])
    #pragma acc exit data copyout(s->ry0[0:s->n_beads])
    #pragma acc exit data copyout(s->rz0[0:s->n_beads])
    #pragma acc exit data copyout(s->px[0:s->n_beads])
    #pragma acc exit data copyout(s->py[0:s->n_beads])
    #pragma acc exit data copyout(s->pz[0:s->n_beads])
    #pragma acc exit data copyout(s->e[0:s->n_beads])
    #pragma acc exit data copyout(s->fx[0:s->n_beads])
    #pragma acc exit data copyout(s->fy[0:s->n_beads])
    #pragma acc exit data copyout(s->fz[0:s->n_beads])
    #pragma acc exit data copyout(s->bead_type[0:s->n_beads])
    #pragma acc exit data copyout(s->c[0:s->n_bead_types])
    #pragma acc exit data copyout(s->m_inv[0:s->n_bead_types])
    #pragma acc exit data copyout(s->gamma[0:s->n_bead_types*s->n_bead_types])
    #pragma acc exit data copyout(s->f_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc exit data copyout(s->f_non_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc exit data copyout(s->kappa_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc exit data copyout(s->kappa_non_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc exit data copyout(s->phi[0:s->n_bead_types*s->n_cells])
    #pragma acc exit data copyout(s->cl[0:s->n_cells*s->cl_len_max])
    #pragma acc exit data copyout(s->bl[0:s->n_beads*s->bl_len_max])
    #pragma acc exit data copyout(s->nl[0:s->n_beads*14*s->cl_len_max])
    #pragma acc exit data copyout(s->cl_len[0:s->n_cells])
    #pragma acc exit data copyout(s->bl_len[0:s->n_beads])
    #pragma acc exit data copyout(s->nl_len[0:s->n_beads*14])
    #pragma acc exit data copyout(s->cell_neighbor[0:s->n_cells*14])
    #pragma acc exit data copyout(s->poly_offset[0:s->n_polys])
    #pragma acc exit data copyout(s->poly_N[0:s->n_polys])
    #pragma acc exit data copyout(s->bl_set[0:s->bl_n_sets*s->bl_set_len_max])
    #pragma acc exit data copyout(s->bl_set_len[0:s->bl_n_sets])
    #pragma acc exit data copyout(s->nl_set[0:s->nl_n_sets*s->nl_set_len_max])
    #pragma acc exit data copyout(s->nl_set_len[0:s->nl_n_sets])
    #pragma acc exit data copyout(s->pcgstate[0:s->n_rngs])
    #pragma acc exit data copyout(s[0:1])

    free(s->rx);
    free(s->ry);
    free(s->rz);
    free(s->px);
    free(s->py);
    free(s->pz);
    free(s->e);
    free(s->fx);
    free(s->fy);
    free(s->fz);
    free(s->rx0);
    free(s->ry0);
    free(s->rz0);
    free(s->bead_type);
    free(s->m_inv);
    free(s->c);
    free(s->gamma);
    free(s->f_bonded);
    free(s->f_non_bonded);
    free(s->kappa_bonded);
    free(s->kappa_non_bonded);
    free(s->phi);
    free(s->cl);
    free(s->nl);
    free(s->bl);
    free(s->cl_len);
    free(s->nl_len);
    free(s->bl_len);
    free(s->cell_neighbor);
    free(s->poly_offset);
    free(s->poly_N);
    free(s->bl_set);
    free(s->bl_set_len);
    free(s->nl_set);
    free(s->nl_set_len);
    free(s->pcgstate);

    H5Fclose(s->ana_file_id);
    #endif
}

void read_conf_arrays(SYSTEM * s)
{
    hid_t file_id = H5Fopen(s->conf_file_name, H5F_ACC_RDONLY, H5P_DEFAULT);

    read_dataset(file_id, s->rx, "rx", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->ry, "ry", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->rz, "rz", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->px, "px", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->py, "py", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->pz, "pz", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->e, "e", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->m_inv, "m_inv", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->c, "c", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->bl, "bl", H5T_NATIVE_INT);
    read_dataset(file_id, s->bl_len, "bl_len", H5T_NATIVE_INT);
    read_dataset(file_id, s->poly_offset, "poly_offset", H5T_NATIVE_INT);
    read_dataset(file_id, s->poly_N, "poly_N", H5T_NATIVE_INT);
    read_dataset(file_id, s->bead_type, "bead_type", H5T_NATIVE_INT);
    read_dataset(file_id, s->f_bonded, "f_bonded", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->f_non_bonded, "f_non_bonded", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->gamma, "gamma", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->kappa_bonded, "kappa_bonded", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->kappa_non_bonded, "kappa_non_bonded", H5T_NATIVE_DOUBLE);


    H5Fclose(file_id);
}

void read_conf_scalars(SYSTEM * s)
{
    hid_t file_id = H5Fopen(s->conf_file_name, H5F_ACC_RDONLY, H5P_DEFAULT);

    read_dataset(file_id, &s->n_beads, "n_beads", H5T_NATIVE_INT);
    read_dataset(file_id, &s->n_cells, "n_cells", H5T_NATIVE_INT);
    read_dataset(file_id, &s->n_polys, "n_polys", H5T_NATIVE_INT);
    read_dataset(file_id, &s->bl_len_max, "bl_len_max", H5T_NATIVE_INT);
    read_dataset(file_id, &s->n_bead_types, "n_bead_types", H5T_NATIVE_INT);
    read_dataset(file_id, &s->lx, "lx", H5T_NATIVE_INT);
    read_dataset(file_id, &s->ly, "ly", H5T_NATIVE_INT);
    read_dataset(file_id, &s->lz, "lz", H5T_NATIVE_INT);
    read_dataset(file_id, &s->dt, "dt", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, &s->info_dt, "info_dt", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, &s->time, "time", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, &s->conf_save_dt_double, "conf_save_dt_double", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, &s->ana_dt_double, "ana_dt_double", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, &s->walltime, "walltime", H5T_NATIVE_INT);

    H5Fclose(file_id);
}

void save_conf(SYSTEM * s)
{
    char fn[1024];
    hsize_t d1[1] = {1}, dm[1] = {s->n_beads}, dp[1] = {s->n_polys}, dt[1] = {s->n_bead_types}, dt2[2] = {s->n_bead_types, s->n_bead_types}, dbl[2] = {s->n_beads, s->bl_len_max};
    int dummy;

    sprintf(fn, "%s_conf_t%lg.h5", s->prefix, s->time);
    hid_t file_id = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    write_dataset(file_id, s->rx, "rx", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->ry, "ry", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->rz, "rz", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->px, "px", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->py, "py", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->pz, "pz", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->e, "e", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->bead_type, "bead_type", 1, dm, H5T_NATIVE_INT);
    write_dataset(file_id, s->bl, "bl", 1, dbl, H5T_NATIVE_INT);
    write_dataset(file_id, s->bl_len, "bl_len", 1, dm, H5T_NATIVE_INT);
    write_dataset(file_id, s->poly_offset, "poly_offset", 1, dp, H5T_NATIVE_INT);
    write_dataset(file_id, s->poly_N, "poly_N", 1, dp, H5T_NATIVE_INT);
    write_dataset(file_id, s->m_inv, "m_inv", 1, dt, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->c, "c", 1, dt, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->f_bonded, "f_bonded", 2, dt2, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->f_non_bonded, "f_non_bonded", 2, dt2, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->gamma, "gamma", 2, dt2, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->kappa_bonded, "kappa_bonded", 2, dt2, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->kappa_non_bonded, "kappa_non_bonded", 2, dt2, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, &s->n_beads, "n_beads", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->n_cells, "n_cells", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->n_polys, "n_polys", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->bl_len_max, "bl_len_max", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->n_bead_types, "n_bead_types", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->lx, "lx", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->ly, "ly", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->lz, "lz", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->dt, "dt", 1, d1, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, &s->time, "time", 1, d1, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, &s->conf_save_dt_double, "conf_save_dt_double", 1, d1, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, &s->ana_dt_double, "ana_dt_double", 1, d1, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, &s->walltime, "walltime", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->info_dt, "info_dt", 1, d1, H5T_NATIVE_DOUBLE);

    H5Fclose(file_id);

    H5Fflush(s->ana_file_id, H5F_SCOPE_GLOBAL);
}

void write_dataset(hid_t file_id, void *data, char *name, int rank, hsize_t * current_dims, hid_t dtype_id)
{
    hid_t dataspace_id = H5Screate_simple(rank, current_dims, NULL);
    hid_t dataset_id = H5Dcreate2(file_id, name, dtype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset_id, dtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);
}

void read_dataset(hid_t file_id, void *data, char *name, hid_t dtype_id)
{
    hid_t dataset_id = H5Dopen2(file_id, name, H5P_DEFAULT);
    H5Dread(dataset_id, dtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
}

int extent_ana_by_field(double *data, uint64_t n_data, char *name, hid_t file_id)
{
    hid_t dset = H5Dopen(file_id, name, H5P_DEFAULT);
    hid_t d_space = H5Dget_space(dset);
    unsigned int ndims = H5Sget_simple_extent_ndims(d_space);
    if(ndims != 2)
    {
        fprintf(stderr, "ERROR: %s:%d not the correct number of dimensions to extent the data set for %s (ndims = %u).\n", __FILE__, __LINE__, name, ndims);
        return -1;
    }
    hsize_t dims[2];//ndims

    H5Sget_simple_extent_dims(d_space, dims, NULL);

    hsize_t dims_new[2];//ndims
    dims_new[0] = dims[0] + 1;
    dims_new[1] = n_data;

    H5Dset_extent(dset, dims_new);

    H5Sclose(d_space);
    hid_t filespace = H5Dget_space(dset);

    hsize_t dims_memspace[2]; //ndims
    dims_memspace[0] = dims_new[0] - dims[0];
    for(unsigned int i = 1; i < ndims; i++){
        dims_memspace[i] = dims[i];
    }

    hid_t memspace = H5Screate_simple(ndims, dims_memspace, NULL);
    hsize_t dims_offset[2];//ndims
    dims_offset[0] = dims[0];
    for(unsigned int i = 1; i < ndims; i++)
    dims_offset[i] = 0;

    if(dims[0] > 0)
    {
        H5Sselect_hyperslab(filespace, H5S_SELECT_SET, dims_offset, NULL, dims_memspace, NULL);
    }
    else
    {
        memspace = H5P_DEFAULT;
    }

    H5Dwrite(dset, H5T_NATIVE_DOUBLE, memspace, filespace, H5P_DEFAULT, data);
    H5Sclose(filespace);
    H5Dclose(dset);
    return 0;
}

int extent_ana_by_phi(int *data, uint64_t n_data, char *name, hid_t file_id)
{
    hid_t dset = H5Dopen(file_id, name, H5P_DEFAULT);
    hid_t d_space = H5Dget_space(dset);
    unsigned int ndims = H5Sget_simple_extent_ndims(d_space);
    if(ndims != 2)
    {
        fprintf(stderr, "ERROR: %s:%d not the correct number of dimensions to extent the data set for %s (ndims = %u).\n", __FILE__, __LINE__, name, ndims);
        return -1;
    }
    hsize_t dims[2];//ndims

    H5Sget_simple_extent_dims(d_space, dims, NULL);

    hsize_t dims_new[2];//ndims
    dims_new[0] = dims[0] + 1;
    dims_new[1] = n_data;

    H5Dset_extent(dset, dims_new);

    H5Sclose(d_space);
    hid_t filespace = H5Dget_space(dset);

    hsize_t dims_memspace[2]; //ndims
    dims_memspace[0] = dims_new[0] - dims[0];
    for(unsigned int i = 1; i < ndims; i++){
        dims_memspace[i] = dims[i];
    }

    hid_t memspace = H5Screate_simple(ndims, dims_memspace, NULL);
    hsize_t dims_offset[2];//ndims
    dims_offset[0] = dims[0];
    for(unsigned int i = 1; i < ndims; i++)
    dims_offset[i] = 0;

    if(dims[0] > 0)
    {
        H5Sselect_hyperslab(filespace, H5S_SELECT_SET, dims_offset, NULL, dims_memspace, NULL);
    }
    else
    {
        memspace = H5P_DEFAULT;
    }

    H5Dwrite(dset, H5T_NATIVE_INT, memspace, filespace, H5P_DEFAULT, data);
    H5Sclose(filespace);
    H5Dclose(dset);
    return 0;
}

void write_simulation_data(SYSTEM * s)
{
    long t = (long)nearbyintl(s->time / s->dt);

    if(t % s->conf_save_dt_int == 0 || s->time_step == s->sim_time_int)
    {
        save_conf(s);
    }

    if(t % s->ana_dt_int == 0)
    {
        ana(s);
    }
}

int print_info(SYSTEM * s, struct timespec time_ref)
{
    struct timespec time_current;
    static struct timespec time_last = {0, 0};
    static int t_last = 0;
    double elapsed_time, tps;

    clock_gettime(CLOCK_REALTIME, &time_current);
    time_current.tv_sec -= time_ref.tv_sec;
    time_current.tv_nsec -= time_ref.tv_nsec;
    elapsed_time = time_current.tv_sec - time_last.tv_sec + (time_current.tv_nsec - time_last.tv_nsec) / 1e9;
    if(elapsed_time > s->info_dt)
    {
        tps = (s->time_step - t_last) * s->dt / elapsed_time;
        printf("INFO: Running for %d s | Simulation time: %lg (%lg %%) | TPS = %lg\n", (int) (time_current.tv_sec), s->time, (double) s->time_step / s->sim_time_int * 100, tps);
        t_last = s->time_step;
        time_last.tv_sec = time_current.tv_sec;
        time_last.tv_nsec = time_current.tv_nsec;
    }
    return (int)time_current.tv_sec;
}
