#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "system.h"
#include "io.h"
#include "integrate.h"

int main(int argc, char *argv[])
{
    struct timespec time_start;
    SYSTEM sys;
    SYSTEM * const s = &sys;
    int run_time = 0;

    if(initialize(s, argc, argv) == -1)
    {
        return -1;
    }

    clock_gettime(CLOCK_REALTIME, &time_start);
    s->time_step = 0;
    for(int t = 0; t < s->sim_time_int && run_time < s->walltime && NoKeyboardInterrupt; t++)
    {
        write_simulation_data(s);
        integrate(s);
        run_time = print_info(s, time_start);
    }

    s->time_0 = s->time;
    write_simulation_data(s);

    printf("runtime = %d\n", run_time);

    finalize(s);
    return 0;
}
