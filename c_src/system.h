#include "pcg.h"
#include <hdf5.h>

typedef struct PAIR
{
    int i, j;
} PAIR;

typedef struct SYSTEM
{
    double * rx;
    double * ry;
    double * rz;
    double * rx0;
    double * ry0;
    double * rz0;
    double * px;
    double * py;
    double * pz;
    double * fx;
    double * fy;
    double * fz;
    double * e;
    double * m_inv;
    double * c;
    double * gamma;
    double * f_bonded;
    double * f_non_bonded;
    double * kappa_bonded;
    double * kappa_non_bonded;
    int * bead_type;
    int * bl;
    int * bl_len;
    PAIR * bl_set;
    int * bl_set_len;
    int * cl;
    int * cl_len;
    int * nl;
    int * nl_len;
    PAIR * nl_set;
    int * nl_set_len;
    int * cell_neighbor;
    int * poly_offset;
    int * poly_N;

    double bead_msd;
    double bead_msd_x;
    double bead_msd_y;
    double bead_msd_z;
    double poly_msd;
    double poly_msd_x;
    double poly_msd_y;
    double poly_msd_z;
    double re2;
    double re2_x;
    double re2_y;
    double re2_z;

    int * phi;
    char * prefix;
    char * conf_file_name;
    char * ana_file_name;

    double dt;
    double info_dt;
    double sim_time_double;
    double conf_save_dt_double;
    double ana_dt_double;
    double time;
    double time_0;
    double acc;
    double acc_bl;
    double acc_nl;
    double e_bl;
    double e_nl;
    double e_int;
    double e_kin;
    double e_tot;
    double e_delta;
    double px_tot;
    double py_tot;
    double pz_tot;
    double T_kin;
    double T_int;
    double cl_mean;
    double cl_std;
    double nl_mean;
    double nl_std;

    int lx;
    int ly;
    int lz;
    int n_beads;
    int n_cells;
    int n_polys;
    int n_bead_types;
    int bl_n_sets;
    int bl_set_len_max;
    int nl_n_sets;
    int nl_set_len_max;
    int sim_time_int;
    int conf_save_dt_int;
    int ana_dt_int;
    int walltime;
    int bl_len_max;
    int cl_len_max;
    long n_acc;
    long n_acc_bl;
    long n_acc_nl;
    long n_mc;
    long n_mc_bl;
    long n_mc_nl;
    int time_step;
    int n_rngs;

    hid_t ana_file_id;
    PCG_STATE pcgstate_main, *pcgs_main;
    PCG_STATE *pcgstate;
} SYSTEM;
