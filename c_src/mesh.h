#pragma acc routine seq
int mod(int a, int b);
#pragma acc routine seq
int int_floor(double x);
#pragma acc routine seq
double wrap_back_pos(double x, double l);
#pragma acc routine seq
double wrap_back_dis(double d, double l);
#pragma acc routine seq
int id_cell(SYSTEM *s, int i);
void calc_cell_neighbor(SYSTEM *s);
