#include "system.h"
#include "integrate.h"
#include "pcg.h"
#include "mesh.h"

void integrate(SYSTEM * s)
{
    vv_step_one(s);
    fill_cl(s);
    fill_nl(s);
    bonded_forces_set(s);
    non_bonded_forces_set(s);
    vv_step_two(s);
    emc_random_pairs(s);
    s->time_step++;
    s->time = s->time_0 + s->dt * s->time_step;
}

void vv_step_one(SYSTEM * s)
{
    #pragma omp parallel for
    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        double dt_m_inv = s->m_inv[s->bead_type[i]] * s->dt;
        s->rx[i] += (s->px[i] + s->fx[i] * s->dt * 0.5) * dt_m_inv;
        s->ry[i] += (s->py[i] + s->fy[i] * s->dt * 0.5) * dt_m_inv;
        s->rz[i] += (s->pz[i] + s->fz[i] * s->dt * 0.5) * dt_m_inv;
        s->px[i] += s->fx[i] * 0.5 * dt_m_inv;
        s->py[i] += s->fy[i] * 0.5 * dt_m_inv;
        s->pz[i] += s->fz[i] * 0.5 * dt_m_inv;
        s->fx[i] = 0.;
        s->fy[i] = 0.;
        s->fz[i] = 0.;
    }
}

void vv_step_two(SYSTEM * s)
{
    #pragma omp parallel for
    #pragma acc parallel loop present(s[0])
    for(int i = 0; i < s->n_beads; i++)
    {
        double dt_m_inv = s->m_inv[s->bead_type[i]] * s->dt;
        s->px[i] += 0.5 * s->fx[i] * dt_m_inv;
        s->py[i] += 0.5 * s->fy[i] * dt_m_inv;
        s->pz[i] += 0.5 * s->fz[i] * dt_m_inv;
    }
}

void fill_cl(SYSTEM * s)
{

    #pragma acc update host(s->rx[0:s->n_beads])
    #pragma acc update host(s->ry[0:s->n_beads])
    #pragma acc update host(s->rz[0:s->n_beads])

    for(int c = 0; c < s->n_cells; c++)
    {
        s->cl_len[c] = 0;
    }

    for(int i = 0; i < s->n_beads; i++)
    {
        int c = id_cell(s, i);
        s->cl_len[c]++;
    }

    int max = 0;
    for(int c = 0; c < s->n_cells; c++)
    {
        if(s->cl_len[c] > max)
        {
            max = s->cl_len[c];
        }
    }
    if(max > s->cl_len_max)
    {
        #pragma acc exit data copyout(s->cl[0:s->n_cells*s->cl_len_max])
        #pragma acc exit data copyout(s->nl[0:s->n_beads*14*s->cl_len_max])
        s->cl_len_max = max;
        #pragma acc update device(s->cl_len_max)
        free(s->cl);
        free(s->nl);
        s->cl = (int*)malloc(s->n_cells * s->cl_len_max * sizeof(int));
        s->nl = (int*)malloc(s->n_beads * 14 * s->cl_len_max * sizeof(int));
        #pragma acc enter data copyin(s->cl[0:s->n_cells*s->cl_len_max])
        #pragma acc enter data copyin(s->nl[0:s->n_beads*14*s->cl_len_max])
    }

    for(int c = 0; c < s->n_cells; c++)
    {
        s->cl_len[c] = 0;
    }

    for(int i = 0; i < s->n_beads; i++)
    {
        int c = id_cell(s, i);
        s->cl[c * s->cl_len_max + s->cl_len[c]] = i;
        s->cl_len[c]++;
    }

    #pragma acc update device(s->cl[0:s->n_cells*s->cl_len_max])
    #pragma acc update device(s->cl_len[0:s->n_cells])

}

void fill_nl(SYSTEM * s)
{
    #pragma omp parallel for
    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        int c = id_cell(s, i);
        for(int cn0 = 0; cn0 < 14; cn0++)
        {
            int n = 0;
            int cn = s->cell_neighbor[c * 14 + cn0];
            for(int j0 = 0; j0 < s->cl_len[cn]; j0++)
            {
                int j = s->cl[cn * s->cl_len_max + j0];
                if(cn != c || j < i)
                {
                    double dx = wrap_back_dis(s->rx[i] - s->rx[j], s->lx);
                    double dy = wrap_back_dis(s->ry[i] - s->ry[j], s->ly);
                    double dz = wrap_back_dis(s->rz[i] - s->rz[j], s->lz);
                    double r2 = dx * dx + dy * dy + dz * dz;

                    if(r2 < 1. && r2 > 1e-10)
                    {
                        s->nl[(i * 14 + cn0) * s->cl_len_max + n] = j;
                        n++;
                    }
                }

            }
            s->nl_len[i * 14 + cn0] = n;
        }
    }
}

void bonded_forces(SYSTEM * s)
{
    for(int i = 0; i < s->n_beads; i++)
    {
        int type_i = s->bead_type[i];

        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            int j = s->bl[i * s->bl_len_max + j0];
            int type_j = s->bead_type[j];
            double dx = s->rx[i] - s->rx[j];
            double dy = s->ry[i] - s->ry[j];
            double dz = s->rz[i] - s->rz[j];
            double f = s->f_bonded[type_i * s->n_bead_types + type_j];
            s->fx[i] -= f * dx;
            s->fy[i] -= f * dy;
            s->fz[i] -= f * dz;
            s->fx[j] += f * dx;
            s->fy[j] += f * dy;
            s->fz[j] += f * dz;
        }
    }
}

void non_bonded_forces(SYSTEM * s)
{
    for(int i = 0; i < s->n_beads; i++)
    {
        int type_i = s->bead_type[i];
        for(int cn0 = 0; cn0 < 14; cn0++)
        {
            for(int j0 = 0; j0 < s->nl_len[i * 14 + cn0]; j0++)
            {
                int j = s->nl[(i * 14 + cn0) * s->cl_len_max + j0];
                int type_j = s->bead_type[j];
                double dx = wrap_back_dis(s->rx[i] - s->rx[j], s->lx);
                double dy = wrap_back_dis(s->ry[i] - s->ry[j], s->ly);
                double dz = wrap_back_dis(s->rz[i] - s->rz[j], s->lz);
                double r_inv = 1. / sqrt(dx * dx + dy * dy + dz * dz);
                double f = s->f_non_bonded[type_i * s->n_bead_types + type_j] * (r_inv - 1.);
                s->fx[i] += f * dx;
                s->fy[i] += f * dy;
                s->fz[i] += f * dz;
                s->fx[j] -= f * dx;
                s->fy[j] -= f * dy;
                s->fz[j] -= f * dz;
            }
        }
    }
}


void emc_random_pairs(SYSTEM * s)
{
    if(rng_uniform(s->pcgs_main) < 0.5)
    {
        emc_random_pairs_bl_set(s);
        emc_random_pairs_nl_set(s);
    }
    else
    {
        emc_random_pairs_nl_set(s);
        emc_random_pairs_bl_set(s);
    }
}

void emc_random_pairs_bl(SYSTEM * s)
{
    int n_mc = 0;
    int n_acc = 0;

    int n_pairs = 0;

    for(int i = 0; i < s->n_beads; i++)
    {
        n_pairs += s->bl_len[i];
    }

    for(int pair = 0; pair < n_pairs; pair++)
    {
        int i = rng_uniform_int(s->n_beads, s->pcgs_main);
        int p = s->bl_len[i];

        if(p > 0)
        {
            int j0 = rng_uniform_int(p, s->pcgs_main);
            int j = s->bl[i * s->bl_len_max + j0];
            emc_inner_bl(s, i, j, &s->pcgstate[0]);
        }
        else
        {
            pair--;
        }
    }

    s->n_mc_bl += n_mc;
    s->n_acc_bl += n_acc;
}

void emc_random_pairs_nl(SYSTEM * s)
{
    int n_mc = 0;
    int n_acc = 0;

    int n_pairs = 0;

    for(int i = 0; i < s->n_beads; i++)
    {
        for(int cn0 = 0; cn0 < 14; cn0++)
        {
            n_pairs += s->nl_len[i * 14 + cn0];
        }
    }

    for(int pair = 0; pair < n_pairs; pair++)
    {
        int i = rng_uniform_int(s->n_beads, s->pcgs_main);
        int cn0 = rng_uniform_int(14, s->pcgs_main);
        int p = s->nl_len[i * 14 + cn0];

        if(p > 0)
        {
            int j0 = rng_uniform_int(p, s->pcgs_main);
            int j = s->nl[(i * 14 + cn0) * s->cl_len_max + j0];
            emc_inner_nl(s, i, j, &s->pcgstate[0]);
        }
        else
        {
            pair--;
        }
    }

    s->n_mc_nl += n_mc;
    s->n_acc_nl += n_acc;
}

int emc_inner_nl(const SYSTEM * const s, const int i1, const int i2, PCG_STATE * const pcgs)
{
    const int type1 = s->bead_type[i1];
    const int type2 = s->bead_type[i2];
    const double g = s->gamma[type1 * s->n_bead_types + type2];
    const double k = s->kappa_non_bonded[type1 * s->n_bead_types + type2];
    const double c1 = s->c[type1];
    const double c2 = s->c[type2];
    const double e1 = s->e[i1];
    const double e2 = s->e[i2];
    double dx = wrap_back_dis(s->rx[i1] - s->rx[i2], s->lx);
    double dy = wrap_back_dis(s->ry[i1] - s->ry[i2], s->ly);
    double dz = wrap_back_dis(s->rz[i1] - s->rz[i2], s->lz);
    const double r = sqrt(dx * dx + dy * dy + dz * dz);
    dx /= r;
    dy /= r;
    dz /= r;
    const double w = 1. - r;
    const double m1_inv = s->m_inv[type1];
    const double m2_inv = s->m_inv[type2];
    const double mu_inv = m1_inv + m2_inv;
    const double T1_inv = c1 / e1;
    const double T2_inv = c2 / e2;
    const double v = m1_inv * (s->px[i1] * dx + s->py[i1] * dy + s->pz[i1] * dz) - m2_inv * (s->px[i2] * dx + s->py[i2] * dy + s->pz[i2] * dz);
    const double sig = sqrt(4. * g / (T1_inv + T2_inv));
    const double2 xi = rng_normal(pcgs);
    const double dt = s->dt;
    const double dv = -g * w * w * mu_inv * v * dt + sig * w * mu_inv * xi.r1 * sqrt(dt);
    const double v_new = v + dv;
    const double de_kin = 0.25 / mu_inv * (v_new * v_new - v * v);
    const double de_cond = k * (T1_inv - T2_inv) * w * w * dt;
    const double de_rand = sqrt(2. * k * dt) * w * xi.r2;
    const double e1_new = e1 - de_kin + de_cond + de_rand;
    const double e2_new = e2 - de_kin - de_cond - de_rand;

    if(e1_new > 0 && e2_new > 0)
    {
        const double T1_inv_new = c1 / e1_new;
        const double T2_inv_new = c2 / e2_new;
        const double sig_new = sqrt(4. * g / (T1_inv_new + T2_inv_new));
        const double xi_p_rev = (-dv + g * w * w * mu_inv * v_new * dt) / (sig_new * w * mu_inv * sqrt(dt));
        const double de = e1_new - e1;
        const double xi_e_rev = (-de - de_kin - k * (T1_inv_new - T2_inv_new) * w * w * dt) / (sqrt(2. * k * dt) * w);
        double p_prop = log(sig / sig_new) - 0.5 * (xi_p_rev * xi_p_rev - xi.r1 * xi.r1 + xi_e_rev * xi_e_rev - xi.r2 * xi.r2);
        p_prop += c1 * log(e1_new / e1) + c2 * log(e2_new / e2);

        if(p_prop >= 0. || exp(p_prop) > rng_uniform(pcgs))
        {
            const double f = (v_new - v) / mu_inv;
            s->px[i1] += f * dx;
            s->py[i1] += f * dy;
            s->pz[i1] += f * dz;
            s->px[i2] -= f * dx;
            s->py[i2] -= f * dy;
            s->pz[i2] -= f * dz;
            s->e[i1] = e1_new;
            s->e[i2] = e2_new;
            return 1;
        }
    }
    return 0;
}

void bonded_forces_set(SYSTEM * s)
{
    for(int i_set = 0; i_set < s->bl_n_sets; i_set++)
    {
        #pragma omp parallel for
        #pragma acc parallel loop present(s[0:1])
        for(int pair0 = 0; pair0 < s->bl_set_len[i_set]; pair0++)
        {
             PAIR pair = s->bl_set[i_set * s->bl_set_len_max + pair0];
             int i = pair.i;
             int j = pair.j;

             int type_i = s->bead_type[i];
             int type_j = s->bead_type[j];
             double dx = s->rx[i] - s->rx[j];
             double dy = s->ry[i] - s->ry[j];
             double dz = s->rz[i] - s->rz[j];
             double f = s->f_bonded[type_i * s->n_bead_types + type_j];
             s->fx[i] -= f * dx;
             s->fy[i] -= f * dy;
             s->fz[i] -= f * dz;
             s->fx[j] += f * dx;
             s->fy[j] += f * dy;
             s->fz[j] += f * dz;
        }
    }
}

int emc_inner_bl(SYSTEM * s, int i1, int i2, PCG_STATE * pcgs)
{
    int type1 = s->bead_type[i1];
    int type2 = s->bead_type[i2];
    double k = s->kappa_bonded[type1 * s->n_bead_types + type2];
    double c1 = s->c[type1];
    double c2 = s->c[type2];
    double e1 = s->e[i1];
    double e2 = s->e[i2];
    double T1_inv = c1 / e1;
    double T2_inv = c2 / e2;
    double2 xi = rng_normal(pcgs);
    double dt = s->dt;
    double de_cond = k * (T1_inv - T2_inv) * dt;
    double de_rand = sqrt(2. * k * dt) * xi.r2;
    double e1_new = e1 + de_cond + de_rand;
    double e2_new = e2 - de_cond - de_rand;

    if(e1_new > 0 && e2_new > 0)
    {
        double T1_inv_new = c1 / e1_new;
        double T2_inv_new = c2 / e2_new;
        double de = e1_new - e1;
        double xi_e_rev = (-de - k * (T1_inv_new - T2_inv_new) * dt) / (sqrt(2. * k * dt));
        double p_prop = - 0.5 * (xi_e_rev * xi_e_rev - xi.r2 * xi.r2);
        p_prop += c1 * log(e1_new / e1) + c2 * log(e2_new / e2);

        if(p_prop >= 0. || exp(p_prop) > rng_uniform(pcgs))
        {
            s->e[i1] = e1_new;
            s->e[i2] = e2_new;
            return 1;
        }
    }
    return 0;
}

void non_bonded_forces_set(SYSTEM * s)
{
    for(int i_set = 0; i_set < s->nl_n_sets; i_set++)
    {
        #pragma omp parallel for
        #pragma acc parallel loop present(s[0:1])
        for(int pair0 = 0; pair0 < s->nl_set_len[i_set]; pair0++)
        {
             PAIR pair = s->nl_set[i_set * s->nl_set_len_max + pair0];
             int c = pair.i;
             int cn0 = pair.j;
             for(int i0 = 0; i0 < s->cl_len[c]; i0++)
             {
                 int i = s->cl[c * s->cl_len_max + i0];
                 int type_i = s->bead_type[i];
                 for(int j0 = 0; j0 < s->nl_len[i * 14 + cn0]; j0++)
                 {
                     int j = s->nl[(i * 14 + cn0) * s->cl_len_max + j0];
                     int type_j = s->bead_type[j];
                     double dx = wrap_back_dis(s->rx[i] - s->rx[j], s->lx);
                     double dy = wrap_back_dis(s->ry[i] - s->ry[j], s->ly);
                     double dz = wrap_back_dis(s->rz[i] - s->rz[j], s->lz);
                     double r_inv = 1. / sqrt(dx * dx + dy * dy + dz * dz);
                     double f = s->f_non_bonded[type_i * s->n_bead_types + type_j] * (r_inv - 1.);
                     s->fx[i] += f * dx;
                     s->fy[i] += f * dy;
                     s->fz[i] += f * dz;
                     s->fx[j] -= f * dx;
                     s->fy[j] -= f * dy;
                     s->fz[j] -= f * dz;
                 }
             }
        }
    }
}

void emc_random_pairs_bl_set(SYSTEM * s)
{
    int n_mc = 0;
    int n_acc = 0;

    for(int i_set0 = 0; i_set0 < s->bl_n_sets; i_set0++)
    {
        int i_set = rng_uniform_int(s->bl_n_sets, s->pcgs_main);

        #pragma omp parallel for reduction(+:n_mc,n_acc)
        #pragma acc parallel loop present(s[0:1]) reduction(+:n_mc,n_acc)
        for(int pair0 = 0; pair0 < s->bl_set_len[i_set]; pair0++)
        {
             PAIR pair = s->bl_set[i_set * s->bl_set_len_max + pair0];
             int i = pair.i;
             int j = pair.j;
             n_acc += emc_inner_bl(s, i, j, &s->pcgstate[pair0]);
             n_mc++;
        }
    }

    s->n_mc_bl += n_mc;
    s->n_acc_bl += n_acc;
}

void emc_random_pairs_nl_set(SYSTEM * const s)
{
    int n_mc = 0;
    int n_acc = 0;

    for(int i_set0 = 0; i_set0 < s->nl_n_sets; i_set0++)
    {
        int i_set = rng_uniform_int(s->nl_n_sets, s->pcgs_main);

        #pragma omp parallel for reduction(+:n_mc,n_acc)
        #pragma acc parallel loop present(s[0:1]) reduction(+:n_mc,n_acc)
        for(int pair0 = 0; pair0 < s->nl_set_len[i_set]; pair0++)
        {
            const PAIR pair = s->nl_set[i_set * s->nl_set_len_max + pair0];
            const int c = pair.i;
            const int cn0 = pair.j;

	    int n_pairs_max = 0;
	    for(int i0 = 0; i0 < s->cl_len[c]; i0++)
            {
                const int i = s->cl[c * s->cl_len_max + i0];
                const int n_pairs = s->nl_len[i * 14 + cn0];
	        if(n_pairs > n_pairs_max)
	        {
                    n_pairs_max = n_pairs;
                }
            }

            for(int i_pair = 0; i_pair < s->cl_len[c] * n_pairs_max; i_pair++)
            {
	        const int i0 = rng_uniform_int(s->cl_len[c], &s->pcgstate[pair0]);
                const int i = s->cl[c * s->cl_len_max + i0];
		const int j0 = rng_uniform_int(n_pairs_max, &s->pcgstate[pair0]);
		if(j0 < s->nl_len[i * 14 + cn0])
		{
                    const int j = s->nl[(i * 14 + cn0) * s->cl_len_max + j0];
                    n_acc += emc_inner_nl(s, i, j, &s->pcgstate[pair0]);
                    n_mc++;
		}
            }
        }
    }

    s->n_mc_nl += n_mc;
    s->n_acc_nl += n_acc;
}
