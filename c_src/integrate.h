void integrate(SYSTEM * s);
void vv_step_one(SYSTEM * s);
void vv_step_two(SYSTEM * s);
void fill_cl(SYSTEM * s);
void fill_nl(SYSTEM * s);
void emc_random_pairs(SYSTEM * s);
void emc_random_pairs_bl(SYSTEM * s);
void emc_random_pairs_nl(SYSTEM * s);
#pragma acc routine seq
int emc_inner_nl(const SYSTEM * const s, const int i1, const int i2, PCG_STATE * const pcgs);
#pragma acc routine seq
int emc_inner_bl(SYSTEM * s, int i1, int i2, PCG_STATE * pcgs);
void non_bonded_forces(SYSTEM * s);
void bonded_forces(SYSTEM * s);

void bonded_forces_set(SYSTEM * s);
void non_bonded_forces_set(SYSTEM * s);
void emc_random_pairs_bl_set(SYSTEM * const s);
void emc_random_pairs_nl_set(SYSTEM * s);
