#include <signal.h>
#include <time.h>

extern volatile int NoKeyboardInterrupt;
void intHandler(int dummy);

int initialize(SYSTEM * s, int argc, char *argv[]);
void finalize(SYSTEM * s);
void read_conf_scalars(SYSTEM * s);
void read_conf_arrays(SYSTEM * s);
void read_dataset(hid_t file_id, void *data, char *name, hid_t dtype_id);
void write_dataset(hid_t file_id, void *data, char *name, int rank, hsize_t * current_dims, hid_t dtype_id);
void save_conf(SYSTEM * s);
int extent_ana_by_field(double *data, uint64_t n_data, char *name, hid_t file_id);
int extent_ana_by_phi(int *data, uint64_t n_data, char *name, hid_t file_id);
void save_ana(SYSTEM * s);
void write_simulation_data(SYSTEM * s);
int print_info(SYSTEM * s, struct timespec time_ref);
