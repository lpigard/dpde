#include <math.h>
#include "system.h"
#include "mesh.h"

int mod(int a, int b)
{
    int r = a % b;
    return r < 0 ? r + b : r;
}

int int_floor(double x)
{
    int i = (int)x; /* truncate */
    return i - ( i > x ); /* convert trunc to floor */
}

double wrap_back_dis(double d, double l)
{
    return d - l * nearbyint(d / l);
}

int id_cell(SYSTEM *s, int i)
{
    int x = mod(int_floor(s->rx[i]), s->lx);
    int y = mod(int_floor(s->ry[i]), s->ly);
    int z = mod(int_floor(s->rz[i]), s->lz);
    return z + s->lz * (y + s->ly * x);
}

void calc_cell_neighbor(SYSTEM *s)
{
    for(int x = 0; x < s->lx; x++)
    {
        for(int y = 0; y < s->ly; y++)
        {
            for(int z = 0; z < s->lz; z++)
            {
                int c = z + s->lz * (y + s->ly * x);
                int n = 0;
                for(int dx = -1; dx <= 1; dx++)
                {
                    for(int dy = -1; dy <= 1; dy++)
                    {
                        for(int dz = -1; dz <= 1; dz++)
                        {
                            if(dz + 3 * (dy + 3 * dx) <= 0)
                            {
                                int xn = x + dx;
                                int yn = y + dy;
                                int zn = z + dz;

                                if(xn < 0)
                                {
                                    xn += s->lx;
                                }
                                else if(xn >= s->lx)
                                {
                                    xn -= s->lx;
                                }

                                if(yn < 0)
                                {
                                    yn += s->ly;
                                }
                                else if(yn >= s->ly)
                                {
                                    yn -= s->ly;
                                }

                                if(zn < 0)
                                {
                                    zn += s->lz;
                                }
                                else if(zn >= s->lz)
                                {
                                    zn -= s->lz;
                                }

                                int cn = zn + s->lz * (yn + s->ly * xn);
                                s->cell_neighbor[c * 14 + n] = cn;
                                n++;
                            }

                        }
                    }
                }
            }
        }
    }
}
