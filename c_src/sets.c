#include <stdio.h>
#include <stdlib.h>
#include "system.h"
#include "sets.h"

int calc_bl_set(SYSTEM *s)
{

    int **pair_is_in_any_set = (int**)malloc(s->n_beads * sizeof(int*)); // needed to check if decomposition in sets is complete, i.e. no pair is missing
    for(int i = 0; i < s->n_beads; i++)
    {
        pair_is_in_any_set[i] = (int*)malloc(s->bl_len_max * sizeof(int));
        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            pair_is_in_any_set[i][j0] = 0;
        }
    }
    int *bead_is_in_current_set = (int*)malloc(s->n_beads * sizeof(int)); // avoid that the same bead is updated in the same set

    int all_pairs_in_sets = 0;
    int set_len_max = 0;
    int n_sets = 0;

    // first loop to calculate number of sets and maximum set length
    do
    {
        int set_len = 0;
        for(int i = 0; i < s->n_beads; i++)
        {
            bead_is_in_current_set[i] = 0;
        }

        for(int i = 0; i < s->n_beads; i++)
        {
            for(int j0 = 0; j0 < s->bl_len[i]; j0++)
            {
                int j = s->bl[i * s->bl_len_max + j0];
                if(bead_is_in_current_set[i] == 0 && bead_is_in_current_set[j] == 0 && pair_is_in_any_set[i][j0] == 0)
                {
                    bead_is_in_current_set[i] = 1;
                    bead_is_in_current_set[j] = 1;
                    pair_is_in_any_set[i][j0] = 1;
                    set_len++;
                }
            }
        }

        if(set_len > set_len_max)
        {
            set_len_max = set_len;
        }

        // check if all pairs are sorted to a set
        all_pairs_in_sets = 1;
        for(int i = 0; i < s->n_beads; i++)
        {
            for(int j0 = 0; j0 < s->bl_len[i]; j0++)
            {
                if(pair_is_in_any_set[i][j0] == 0)
                {
                    all_pairs_in_sets = 0;
                }
            }
        }

        n_sets++;
    }while(all_pairs_in_sets == 0);

    s->bl_n_sets = n_sets;
    s->bl_set_len_max = set_len_max;
    s->bl_set_len = (int*)malloc(s->bl_n_sets * sizeof(int));
    s->bl_set = (PAIR*)malloc(s->bl_n_sets * s->bl_set_len_max * sizeof(PAIR));

    for(int i = 0; i < s->n_beads; i++)
    {
        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            pair_is_in_any_set[i][j0] = 0;
        }
    }

    // second loop to calculate actual sets
    int i_set = 0;
    do
    {
        int set_len = 0;
        for(int i = 0; i < s->n_beads; i++)
        {
            bead_is_in_current_set[i] = 0;
        }

        for(int i = 0; i < s->n_beads; i++)
        {
            for(int j0 = 0; j0 < s->bl_len[i]; j0++)
            {
                int j = s->bl[i * s->bl_len_max + j0];
                if(bead_is_in_current_set[i] == 0 && bead_is_in_current_set[j] == 0 && pair_is_in_any_set[i][j0] == 0)
                {
                    bead_is_in_current_set[i] = 1;
                    bead_is_in_current_set[j] = 1;
                    pair_is_in_any_set[i][j0] = 1;
                    PAIR pair;
                    pair.i = i;
                    pair.j = j;
                    s->bl_set[i_set * s->bl_set_len_max + set_len] = pair;
                    set_len++;
                }
            }
        }

        s->bl_set_len[i_set] = set_len;

        // check if all pairs are sorted to a set
        all_pairs_in_sets = 1;
        for(int i = 0; i < s->n_beads; i++)
        {
            for(int j0 = 0; j0 < s->bl_len[i]; j0++)
            {
                if(pair_is_in_any_set[i][j0] == 0)
                {
                    all_pairs_in_sets = 0;
                }
            }
        }

        i_set++;
    }while(all_pairs_in_sets == 0);

    printf("bl_n_sets = %d (", s->bl_n_sets);
    int sum_set_len = 0;
    for(int i_set = 0; i_set < s->bl_n_sets; i_set++)
    {
        printf("%d ", s->bl_set_len[i_set]);
        sum_set_len += s->bl_set_len[i_set];
    }
    printf(")\n");

    int bl_len_sum = 0;
    for(int i = 0; i < s->n_beads; i++)
    {
        bl_len_sum += s->bl_len[i];
    }

    if(sum_set_len == bl_len_sum)
    {
        printf("number of bonded particle pairs = number of pairs in bonded sets = %d\n", sum_set_len);
    }
    else
    {
        printf("ERROR: number of bonded particle pairs (%d) != number of pairs in bonded sets (%d)", bl_len_sum, sum_set_len);
        return -1;
    }

    for(int i = 0; i < s->n_beads; i++)
    {
        free(pair_is_in_any_set[i]);
    }
    free(pair_is_in_any_set);
    free(bead_is_in_current_set);

    return 0;
}

int calc_nl_set(SYSTEM *s)
{
    int **pair_is_in_any_set = (int**)malloc(s->n_cells * sizeof(int*)); // needed to check if decomposition in sets is complete, i.e. no pair is missing
    for(int c = 0; c < s->n_cells; c++)
    {
        pair_is_in_any_set[c] = (int*)malloc(14 * sizeof(int));
        for(int cn = 0; cn < 14; cn++)
        {
            pair_is_in_any_set[c][cn] = 0;
        }
    }
    int *cell_is_in_current_set = (int*)malloc(s->n_cells * sizeof(int)); // avoid that the same cell is updated in the same set

    int all_pairs_in_sets = 0;
    int set_len_max = 0;
    int n_sets = 0;

    // first loop to calculate number of sets and maximum set length
    do
    {
        int set_len = 0;
        for(int c = 0; c < s->n_cells; c++)
        {
            cell_is_in_current_set[c] = 0;
        }

        for(int x = 0; x < s->lx; x++)
        {
            for(int y = 0; y < s->ly; y++)
            {
                for(int z = 0; z < s->lz; z++)
                {
                    int c = z + s->lz * (y + s->ly * x);
                    for(int cn0 = 0; cn0 < 14; cn0++)
                    {
                        int cn = s->cell_neighbor[c * 14 + cn0];

                        if(cell_is_in_current_set[c] == 0 && cell_is_in_current_set[cn] == 0 && pair_is_in_any_set[c][cn0] == 0)
                        {
                            cell_is_in_current_set[c] = 1;
                            cell_is_in_current_set[cn] = 1;
                            pair_is_in_any_set[c][cn0] = 1;
                            set_len++;
                        }
                    }
                }
            }
        }

        if(set_len > set_len_max)
        {
            set_len_max = set_len;
        }

        // check if all pairs are sorted to a set
        all_pairs_in_sets = 1;
        for(int c = 0; c < s->n_cells; c++)
        {
            for(int cn0 = 0; cn0 < 14; cn0++)
            {
                if(pair_is_in_any_set[c][cn0] == 0)
                {
                    all_pairs_in_sets = 0;
                }
            }
        }

        n_sets++;
    }while(all_pairs_in_sets == 0);

    s->nl_n_sets = n_sets;
    s->nl_set_len_max = set_len_max;
    s->nl_set_len = (int*)malloc(s->nl_n_sets * sizeof(int));
    s->nl_set = (PAIR*)malloc(s->nl_n_sets * s->nl_set_len_max * sizeof(PAIR));

    for(int c = 0; c < s->n_cells; c++)
    {
        for(int cn0 = 0; cn0 < 14; cn0++)
        {
            pair_is_in_any_set[c][cn0] = 0;
        }
    }


    // second loop to calculate actual sets
    int i_set = 0;
    do
    {
        int set_len = 0;
        for(int c = 0; c < s->n_cells; c++)
        {
            cell_is_in_current_set[c] = 0;
        }

        for(int x = 0; x < s->lx; x++)
        {
            for(int y = 0; y < s->ly; y++)
            {
                for(int z = 0; z < s->lz; z++)
                {
                    int c = z + s->lz * (y + s->ly * x);
                    for(int cn0 = 0; cn0 < 14; cn0++)
                    {
                        int cn = s->cell_neighbor[c * 14 + cn0];

                        if(cell_is_in_current_set[c] == 0 && cell_is_in_current_set[cn] == 0 && pair_is_in_any_set[c][cn0] == 0)
                        {
                            cell_is_in_current_set[c] = 1;
                            cell_is_in_current_set[cn] = 1;
                            pair_is_in_any_set[c][cn0] = 1;
                            PAIR pair;
                            pair.i = c;
                            pair.j = cn0;
                            s->nl_set[i_set * s->nl_set_len_max + set_len] = pair;
                            set_len++;
                        }
                    }
                }
            }
        }

        s->nl_set_len[i_set] = set_len;

        // check if all pairs are sorted to a set
        all_pairs_in_sets = 1;
        for(int c = 0; c < s->n_cells; c++)
        {
            for(int cn0 = 0; cn0 < 14; cn0++)
            {
                if(pair_is_in_any_set[c][cn0] == 0)
                {
                    all_pairs_in_sets = 0;
                }
            }
        }

        i_set++;
    }while(all_pairs_in_sets == 0);

    printf("nl_n_sets = %d (", s->nl_n_sets);
    int sum_set_len = 0;
    for(int i_set = 0; i_set < s->nl_n_sets; i_set++)
    {
        printf("%d ", s->nl_set_len[i_set]);
        sum_set_len += s->nl_set_len[i_set];
    }
    printf(")\n");
    if(sum_set_len == s->n_cells * 14)
    {
        printf("number of cell pairs = number of pairs in non-bonded sets = %d\n", sum_set_len);
    }
    else
    {
        printf("ERROR: number of cell pairs (%d) != number of pairs in non-bonded sets (%d)", s->n_cells * 14, sum_set_len);
        return -1;
    }

    for(int c = 0; c < s->n_cells; c++)
    {
        free(pair_is_in_any_set[c]);
    }
    free(pair_is_in_any_set);
    free(cell_is_in_current_set);

    return 0;
}
