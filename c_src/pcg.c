#include "pcg.h"

uint32_t pcg32_random(PCG_STATE * rng)
{
    const uint64_t old = rng->state;
    // Advance internal state
    rng->state = ((uint64_t) rng->state) * 0X5851F42D4C957F2DULL;
    rng->state += (rng->inc | 1);
    const uint32_t xorshifted = ((old >> 18u) ^ old) >> 27u;
    const uint32_t rot = old >> 59u;
    return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}

int seed_pcg32(PCG_STATE * rng, uint64_t seed, uint64_t stream)
{
    rng->inc = stream*2 + 1;
    rng->state = 0;
    pcg32_random(rng);
    rng->state += seed;
    pcg32_random(rng);
    //Improve quality of first random numbers
    pcg32_random(rng);
    return 0;
}

unsigned int pcg_max(void)
{
    return 4294967295U;
}

double rng_uniform(PCG_STATE * pcgs)
{
    return pcg32_random(pcgs) / (double) pcg_max();
}

int rng_uniform_int(int i, PCG_STATE * pcgs)
{
    return (int)(rng_uniform(pcgs) * i) % i;
}

double2 rng_normal(PCG_STATE * pcgs)
{
    double u1, u2, q;
    do{
        u1 = 2. * rng_uniform(pcgs) - 1.;
        u2 = 2. * rng_uniform(pcgs) - 1.;
        q = u1 * u1 + u2 * u2;
    }while(q == 0. || q >= 1.);

    double p = sqrt(-2. * log(q) / q);
    double2 r = {p * u1, p * u2};
    return r;
}
