#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "system.h"
#include "io.h"
#include "mesh.h"
#include "ana.h"

void ana(SYSTEM * s)
{
    printf("\n");

    #pragma acc update host(s->rx[0:s->n_beads])
    #pragma acc update host(s->ry[0:s->n_beads])
    #pragma acc update host(s->rz[0:s->n_beads])
    #pragma acc update host(s->rx0[0:s->n_beads])
    #pragma acc update host(s->ry0[0:s->n_beads])
    #pragma acc update host(s->rz0[0:s->n_beads])
    #pragma acc update host(s->px[0:s->n_beads])
    #pragma acc update host(s->py[0:s->n_beads])
    #pragma acc update host(s->pz[0:s->n_beads])
    #pragma acc update host(s->e[0:s->n_beads])
    #pragma acc update host(s->fx[0:s->n_beads])
    #pragma acc update host(s->fy[0:s->n_beads])
    #pragma acc update host(s->fz[0:s->n_beads])
    #pragma acc update host(s->bead_type[0:s->n_beads])
    #pragma acc update host(s->c[0:s->n_bead_types])
    #pragma acc update host(s->m_inv[0:s->n_bead_types])
    #pragma acc update host(s->gamma[0:s->n_bead_types*s->n_bead_types])
    #pragma acc update host(s->f_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc update host(s->f_non_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc update host(s->kappa_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc update host(s->kappa_non_bonded[0:s->n_bead_types*s->n_bead_types])
    #pragma acc update host(s->phi[0:s->n_bead_types*s->n_cells])
    #pragma acc update host(s->cl[0:s->n_cells*s->cl_len_max])
    #pragma acc update host(s->bl[0:s->n_beads*s->bl_len_max])
    #pragma acc update host(s->nl[0:s->n_beads*14*s->cl_len_max])
    #pragma acc update host(s->cl_len[0:s->n_cells])
    #pragma acc update host(s->bl_len[0:s->n_beads])
    #pragma acc update host(s->nl_len[0:s->n_beads*14])
    #pragma acc update host(s->cell_neighbor[0:s->n_cells])
    #pragma acc update host(s->poly_offset[0:s->n_polys])
    #pragma acc update host(s->poly_N[0:s->n_polys])
    #pragma acc update host(s->bl_set[0:s->bl_n_sets*s->bl_set_len_max])
    #pragma acc update host(s->bl_set_len[0:s->bl_n_sets])
    #pragma acc update host(s->nl_set[0:s->nl_n_sets*s->nl_set_len_max])
    #pragma acc update host(s->nl_set_len[0:s->nl_n_sets])

    ana_time(s);
    ana_energies(s);
    ana_p_tot(s);
    ana_temperature(s);
    ana_mc(s);
    ana_cl(s);
    ana_nl(s);
    ana_bead_msd(s);
    ana_poly_msd(s);
    ana_re2(s);
    ana_phi(s);

    printf("\n");
}

void ana_time(SYSTEM * s)
{
    extent_ana_by_field(&s->time, 1, "time", s->ana_file_id);
    printf("time=%lg", s->time);
}

void ana_energies(SYSTEM * s)
{
    static double e_tot_old = 0.;
    e_tot_old = s->e_tot;

    calc_e_kin(s);
    calc_e_int(s);
    calc_e_bl(s);
    calc_e_nl(s);
    s->e_tot = s->e_kin + s->e_int + s->e_bl + s->e_nl;
    s->e_delta = s->e_tot - e_tot_old;

    extent_ana_by_field(&s->e_kin, 1, "e_kin", s->ana_file_id);
    extent_ana_by_field(&s->e_int, 1, "e_int", s->ana_file_id);
    extent_ana_by_field(&s->e_bl, 1, "e_bl", s->ana_file_id);
    extent_ana_by_field(&s->e_nl, 1, "e_nl", s->ana_file_id);
    extent_ana_by_field(&s->e_tot, 1, "e_tot", s->ana_file_id);
    extent_ana_by_field(&s->e_delta, 1, "e_delta", s->ana_file_id);

    printf(" e_kin=%lg", s->e_kin);
    printf(" e_int=%lg", s->e_int);
    printf(" e_bl=%lg", s->e_bl);
    printf(" e_nl=%lg", s->e_nl);
    printf(" e_tot=%lg(%lg)", s->e_tot, fabs(s->e_delta) / s->e_tot);
}

void calc_e_kin(SYSTEM * s)
{
    s->e_kin = 0.;
    for(int i = 0; i < s->n_beads; i++)
    {
        int type = s->bead_type[i];
        s->e_kin += 0.5 * s->m_inv[type] * (s->px[i] * s->px[i] + s->py[i] * s->py[i] + s->pz[i] * s->pz[i]);
    }
}

void calc_e_int(SYSTEM * s)
{
    s->e_int = 0.;
    for(int i = 0; i < s->n_beads; i++)
    {
        s->e_int += s->e[i];
    }
}

void calc_e_bl(SYSTEM * s)
{
    s->e_bl = 0.;
    for(int i = 0; i < s->n_beads; i++)
    {
        int type_i = s->bead_type[i];
        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            int j = s->bl[i * s->bl_len_max + j0];
            int type_j = s->bead_type[j];
            double dx = s->rx[i] - s->rx[j];
            double dy = s->ry[i] - s->ry[j];
            double dz = s->rz[i] - s->rz[j];
            double f = 0.5 * s->f_bonded[type_i * s->n_bead_types + type_j];
            s->e_bl += f * (dx * dx + dy * dy + dz * dz);
        }
    }
}

void calc_e_nl(SYSTEM * s)
{
    s->e_nl = 0.;
    for(int i = 0; i < s->n_beads; i++)
    {
        int type_i = s->bead_type[i];
        for(int cn0 = 0; cn0 < 14; cn0++)
        {
            for(int j0 = 0; j0 < s->nl_len[i * 14 + cn0]; j0++)
            {
                int j = s->nl[(i * 14 + cn0) * s->cl_len_max + j0];
                int type_j = s->bead_type[j];
                double dx = wrap_back_dis(s->rx[i] - s->rx[j], s->lx);
                double dy = wrap_back_dis(s->ry[i] - s->ry[j], s->ly);
                double dz = wrap_back_dis(s->rz[i] - s->rz[j], s->lz);
                double a = 1 - sqrt(dx * dx + dy * dy + dz * dz);
                s->e_nl += 0.5 * s->f_non_bonded[type_i * s->n_bead_types + type_j] * a * a;
            }
        }
    }
}

void ana_p_tot(SYSTEM * s)
{
    calc_p_tot(s);

    extent_ana_by_field(&s->px_tot, 1, "px_tot", s->ana_file_id);
    extent_ana_by_field(&s->py_tot, 1, "py_tot", s->ana_file_id);
    extent_ana_by_field(&s->pz_tot, 1, "pz_tot", s->ana_file_id);

    printf(" p_tot/n=(%lg,%lg,%lg)", s->px_tot / s->n_beads, s->py_tot / s->n_beads, s->pz_tot / s->n_beads);
}

void calc_p_tot(SYSTEM * s)
{
    s->px_tot = 0.;
    s->py_tot = 0.;
    s->pz_tot = 0.;
    for(int i = 0; i < s->n_beads; i++)
    {
        s->px_tot += s->px[i];
        s->py_tot += s->py[i];
        s->pz_tot += s->pz[i];
    }
}

void ana_temperature(SYSTEM * s)
{
    calc_T_int(s);
    calc_T_kin(s);

    extent_ana_by_field(&s->T_kin, 1, "T_kin", s->ana_file_id);
    extent_ana_by_field(&s->T_int, 1, "T_int", s->ana_file_id);

    printf(" T_int=%lg", s->T_int);
    printf(" T_kin=%lg", s->T_kin);
}

void calc_T_int(SYSTEM * s)
{
    double sum = 0.;
    for(int i = 0; i < s->n_beads; i++)
    {
        int type = s->bead_type[i];
        sum += s->c[type] / s->e[i];
    }
    s->T_int = s->n_beads / sum;
}

void calc_T_kin(SYSTEM * s)
{
    double sum = 0.;
    for(int i = 0; i < s->n_beads; i++)
    {
        int type = s->bead_type[i];
        sum += s->m_inv[type] / 3. * (s->px[i] * s->px[i] + s->py[i] * s->py[i] + s->pz[i] * s->pz[i]);
    }
    s->T_kin = sum / (s->n_beads - 1);
}

void ana_mc(SYSTEM * s)
{
    s->n_mc = s->n_mc_bl + s->n_mc_nl;
    s->n_acc = s->n_acc_bl + s->n_acc_nl;
    s->acc = s->n_mc > 0 ? (double)s->n_acc / s->n_mc : -1;
    s->acc_bl = s->n_mc_bl > 0 ? (double)s->n_acc_bl / s->n_mc_bl : -1;
    s->acc_nl = s->n_mc_nl > 0 ? (double)s->n_acc_nl / s->n_mc_nl : -1;

    double n_mc = (double)s->n_mc / s->n_beads / s->ana_dt_int;
    double n_mc_bl = (double)s->n_mc_bl / s->n_beads / s->ana_dt_int;
    double n_mc_nl = (double)s->n_mc_nl / s->n_beads / s->ana_dt_int;

    extent_ana_by_field(&n_mc, 1, "n_mc", s->ana_file_id);
    extent_ana_by_field(&n_mc_bl, 1, "n_mc_bl", s->ana_file_id);
    extent_ana_by_field(&n_mc_nl, 1, "n_mc_nl", s->ana_file_id);
    extent_ana_by_field(&s->acc, 1, "acc", s->ana_file_id);
    extent_ana_by_field(&s->acc_bl, 1, "acc_bl", s->ana_file_id);
    extent_ana_by_field(&s->acc_nl, 1, "acc_nl", s->ana_file_id);

    printf(" acc=%lg(%lg,%lg)", s->acc, s->acc_bl, s->acc_nl);
    printf(" n_mc/n/t=%lg(%lg,%lg)", n_mc , n_mc_bl, n_mc_nl);

    s->n_mc = 0;
    s->n_mc_bl = 0;
    s->n_mc_nl = 0;
    s->n_acc = 0;
    s->n_acc_bl = 0;
    s->n_acc_nl = 0;
}

void ana_cl(SYSTEM * s)
{
    calc_cl_mean(s);
    calc_cl_std(s);

    extent_ana_by_field(&s->cl_mean, 1, "cl_mean", s->ana_file_id);
    extent_ana_by_field(&s->cl_std, 1, "cl_std", s->ana_file_id);

    printf(" cl=(%lg,%lg)", s->cl_mean, s->cl_std);
}

void calc_cl_mean(SYSTEM * s)
{
    double sum = 0.;
    for(int c = 0; c < s->n_cells; c++)
    {
        sum += s->cl_len[c];
    }
    s->cl_mean = sum / s->n_cells;
}

void calc_cl_std(SYSTEM * s)
{
    double sum = 0.;
    for(int c = 0; c < s->n_cells; c++)
    {
        double tmp = s->cl_len[c] - s->cl_mean;
        sum += tmp * tmp;
    }
    s->cl_std = sqrt(sum / s->n_cells);
}

void ana_nl(SYSTEM * s)
{
    calc_nl_mean(s);
    calc_nl_std(s);

    extent_ana_by_field(&s->nl_mean, 1, "nl_mean", s->ana_file_id);
    extent_ana_by_field(&s->nl_std, 1, "nl_std", s->ana_file_id);

    printf(" nl=(%lg,%lg)", s->nl_mean, s->nl_std);
}

void calc_nl_mean(SYSTEM * s)
{
    double sum = 0.;
    for(int i = 0; i < s->n_beads; i++)
    {
        for(int cn0 = 0; cn0 < 14; cn0++)
        {
            sum += s->nl_len[i * 14 + cn0];
        }
    }
    s->nl_mean = sum / s->n_beads;
}

void calc_nl_std(SYSTEM * s)
{
    double sum = 0.;
    for(int i = 0; i < s->n_beads; i++)
    {
        double sum_i = 0.;
        for(int cn0 = 0; cn0 < 14; cn0++)
        {
            sum_i += s->nl_len[i * 14 + cn0];

        }
        double tmp =  sum_i - s->nl_mean;
        sum += tmp * tmp;
    }
    s->nl_std = sqrt(sum / s->n_beads / 14.);
}

void ana_bead_msd(SYSTEM * s)
{
    calc_bead_msd(s);

    extent_ana_by_field(&s->bead_msd_x, 1, "bead_msd_x", s->ana_file_id);
    extent_ana_by_field(&s->bead_msd_y, 1, "bead_msd_y", s->ana_file_id);
    extent_ana_by_field(&s->bead_msd_z, 1, "bead_msd_z", s->ana_file_id);

    printf(" bead_msd=%lg(%lg,%lg,%lg)", s->bead_msd, s->bead_msd_x, s->bead_msd_y, s->bead_msd_z);
}

void calc_bead_msd(SYSTEM * s)
{
    double sum_x = 0.;
    double sum_y = 0.;
    double sum_z = 0.;
    for(int i = 0; i < s->n_beads; i++)
    {
        double dx = s->rx[i] - s->rx0[i];
        double dy = s->ry[i] - s->ry0[i];
        double dz = s->rz[i] - s->rz0[i];

        sum_x += dx * dx;
        sum_y += dy * dy;
        sum_z += dz * dz;
    }
    s->bead_msd_x = sum_x / s->n_beads;
    s->bead_msd_y = sum_y / s->n_beads;
    s->bead_msd_z = sum_z / s->n_beads;
    s->bead_msd = s->bead_msd_x + s->bead_msd_y + s->bead_msd_z;
}

void ana_poly_msd(SYSTEM * s)
{
    calc_poly_msd(s);

    extent_ana_by_field(&s->poly_msd_x, 1, "poly_msd_x", s->ana_file_id);
    extent_ana_by_field(&s->poly_msd_y, 1, "poly_msd_y", s->ana_file_id);
    extent_ana_by_field(&s->poly_msd_z, 1, "poly_msd_z", s->ana_file_id);

    printf(" poly_msd=%lg(%lg,%lg,%lg)", s->poly_msd, s->poly_msd_x, s->poly_msd_y, s->poly_msd_z);
}

void calc_poly_msd(SYSTEM * s)
{
    double sum_x = 0.;
    double sum_y = 0.;
    double sum_z = 0.;

    for(int p = 0; p < s->n_polys; p++)
    {
        double cm_x = 0.;
        double cm_y = 0.;
        double cm_z = 0.;

        int offset = s->poly_offset[p];
        int N = s->poly_N[p];

        for(int i0 = 0; i0 < N; i0++)
        {
            int i = offset + i0;
            cm_x += s->rx[i] - s->rx0[i];
            cm_y += s->ry[i] - s->ry0[i];
            cm_z += s->rz[i] - s->rz0[i];
        }

        double dx = cm_x / N;
        double dy = cm_y / N;
        double dz = cm_z / N;

        sum_x += dx * dx;
        sum_y += dy * dy;
        sum_z += dz * dz;
    }
    s->poly_msd_x = sum_x / s->n_polys;
    s->poly_msd_y = sum_y / s->n_polys;
    s->poly_msd_z = sum_z / s->n_polys;
    s->poly_msd = s->poly_msd_x + s->poly_msd_y + s->poly_msd_z;
}

void ana_re2(SYSTEM * s)
{
    calc_re2(s);

    extent_ana_by_field(&s->re2_x, 1, "re2_x", s->ana_file_id);
    extent_ana_by_field(&s->re2_y, 1, "re2_y", s->ana_file_id);
    extent_ana_by_field(&s->re2_z, 1, "re2_z", s->ana_file_id);

    printf(" re2=%lg(%lg,%lg,%lg)", s->re2, s->re2_x, s->re2_y, s->re2_z);
}

void calc_re2(SYSTEM * s)
{
    double sum_x = 0.;
    double sum_y = 0.;
    double sum_z = 0.;

    for(int p = 0; p < s->n_polys; p++)
    {
        double cm_x = 0.;
        double cm_y = 0.;
        double cm_z = 0.;

        int offset = s->poly_offset[p];
        int N = s->poly_N[p];

        for(int i0 = 0; i0 < N; i0++)
        {
            int i = offset + i0;
            cm_x += s->rx[i] - s->rx0[i];
            cm_y += s->ry[i] - s->ry0[i];
            cm_z += s->rz[i] - s->rz0[i];
        }

        double dx = s->rx[offset + N - 1] - s->rx[offset];
        double dy = s->ry[offset + N - 1] - s->ry[offset];
        double dz = s->rz[offset + N - 1] - s->rz[offset];

        sum_x += dx * dx;
        sum_y += dy * dy;
        sum_z += dz * dz;
    }
    s->re2_x = sum_x / s->n_polys;
    s->re2_y = sum_y / s->n_polys;
    s->re2_z = sum_z / s->n_polys;
    s->re2 = s->re2_x + s->re2_y + s->re2_z;
}

void ana_phi(SYSTEM * s)
{
    calc_phi(s);

    extent_ana_by_phi(s->phi, s->n_cells * s->n_bead_types, "phi", s->ana_file_id);
}

void calc_phi(SYSTEM * s)
{
    for(int t = 0; t < s->n_bead_types; t++)
    {
        for(int c = 0; c < s->n_cells; c++)
        {
            s->phi[t * s->n_cells + c] = 0;
        }
    }

    for(int i = 0; i < s->n_beads; i++)
    {
        int t = s->bead_type[i];
        int c = id_cell(s, i);
        s->phi[t * s->n_cells + c]++;
    }
}
