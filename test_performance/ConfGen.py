#!/usr/bin/env python

import numpy as np
import sys
import h5py
import random
import argparse

#{"type of polymer": "linear", "number of polymers": 1000, "number of monomers": 32, "type of monomer": 0},
#{"type of polymer": "star", "number of polymers": 1, "number of monomers per arm": 32, "type of arms": 0, "number of arms": 4, "type of core": 1}

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--name',
            action="store", dest="name",
            help="name for .h5 files", default="test")
arguments = parser.parse_args()

# !!!!! INSERT PARAMETERS HERE !!!!!

lx = 20
ly = 20
lz = 20
dt = 1e-2
px_bias = 0
py_bias = 0
pz_bias = 0

poly_list = [
            {"type of polymer": "linear", "number of polymers": 2000, "number of monomers": 16, "type of monomers": 0},
            # {"type of polymer": "star", "number of polymers": 0, "number of monomers per arm": 64, "type of arms": 0, "number of arms": 4, "type of core": 1}
            ]

c = np.array([11], dtype = np.double)
m_inv = np.array([1], dtype = np.double)
bond_length = np.array([[1]], dtype = np.double)
f_non_bonded = np.array([[1]], dtype = np.double)
gamma = np.array([[10]], dtype = np.double)
kappa_bonded = np.array([[1]], dtype = np.double)
kappa_non_bonded = np.array([[10]], dtype = np.double)
conf_save_dt_double = np.double(1e1)
ana_dt_double = np.double(1e0)
info_dt = np.double(10)
walltime = np.intc(3600 * 23)

# !!!!! INSERT PARAMETERS HERE !!!!!

# seed_master = random.randint(0, sys.maxsize)
# random.seed(seed_master)

bead_type_list = []
for pt in poly_list:
    if pt["type of polymer"] == "linear":
        bead_type_list = np.append(bead_type_list, pt["type of monomers"])
    if pt["type of polymer"] == "star":
        bead_type_list = np.append(bead_type_list, pt["type of arms"])
        bead_type_list = np.append(bead_type_list, pt["type of core"])
bead_type_list = np.unique(bead_type_list)
n_bead_types = bead_type_list.size
n_poly_types = len(poly_list)
n_cells = lx * ly * lz

#if lx < 4 or ly < 4 or lz < 4 or lx % 2 != 0 or ly % 2 != 0 or lz % 2 != 0:
#    print('ERROR: box sizes lx, ly, and lz have to larger than 2 and divisible by 2.')

if c.size != n_bead_types:
    print('ERROR: incorrect size of c.')
    exit()

if m_inv.size != n_bead_types:
    print('ERROR: incorrect size of m_inv.')
    exit()

if bond_length.size != n_bead_types**2:
    print('ERROR: incorrect size of bond_length.')
    exit()

if f_non_bonded.size != n_bead_types**2:
    print('ERROR: incorrect size of f_nonbonded.')
    exit()

if kappa_bonded.size != n_bead_types**2:
    print('ERROR: incorrect size of kappa_int_bonded.')
    exit()

if kappa_non_bonded.size != n_bead_types**2:
    print('ERROR: incorrect size of kappa_int_nonbonded.')
    exit()

for t in bead_type_list:
    if not (t >= 0 and t < n_bead_types):
        print('ERROR: type of monomer has to be an integer between 0 and {}'.format(nt))
        exit()

if not np.allclose(bond_length, bond_length.T):
    print("ERROR: bond length matrix not symmetric!")
    exit()

if not np.allclose(f_non_bonded, f_non_bonded.T):
    print("ERROR: f_nonbonded matrix not symmetric!")
    exit()

if not np.allclose(kappa_bonded, kappa_bonded.T):
    print("ERROR: kappa_int_bonded matrix not symmetric!")
    exit()

if not np.allclose(kappa_non_bonded, kappa_non_bonded.T):
    print("ERROR: kappa_int_nonbonded matrix not symmetric!")
    exit()

bead_type = np.array([], dtype=np.intc)
bl_len = np.array([], dtype=np.intc)
bl = np.array([], dtype=np.intc)

pg = 0
n_beads = 0
for pt in poly_list:
    if pt["type of polymer"] == "linear":
        N = pt["number of monomers"]
    if pt["type of polymer"] == "star":
        N = pt["number of arms"] * pt["number of monomers per arm"] + 1

    bead_type_part = np.zeros(N, dtype=np.intc)
    bl_len_part = np.zeros(N, dtype=np.intc)

    if pt["type of polymer"] == "linear":
        for m in range(N):
          bead_type_part[m] = pt["type of monomers"]
        bl_len_part[0] = 0
        for m in range(1, N):
            bl_len_part[m] = 1

    if pt["type of polymer"] == "star":
        for m in range(1, N):
            bead_type_part[m] = pt["type of arms"]
        bead_type_part[0] = pt['type of core']
        bl_len_part[0] = 0
        for a in range(0, pt["number of arms"]):
            for m0 in range(0, pt["number of monomers per arm"]):
                m = 1 + a * pt["number of monomers per arm"] + m0
                bl_len_part[m] = 1

    bl_len = np.append(bl_len, np.tile(bl_len_part, pt['number of polymers']))
    bead_type = np.append(bead_type, np.tile(bead_type_part, pt['number of polymers']))
    pg += pt['number of polymers']
    n_beads += N * pt['number of polymers']

bl_len_max = np.amax(bl_len)
n_polys = pg
bl = np.zeros((n_beads, bl_len_max), dtype = np.intc)
rx = np.zeros(n_beads, dtype=np.double)
ry = np.zeros(n_beads, dtype=np.double)
rz = np.zeros(n_beads, dtype=np.double)
poly_N = np.zeros(n_polys, dtype=np.intc)
poly_offset = np.zeros(n_polys, dtype=np.intc)

mg = 0
pg = 0
for pt in poly_list:
    if pt["type of polymer"] == "linear":
      N = pt["number of monomers"]
    if pt["type of polymer"] == "star":
      N = pt["number of arms"] * pt["number of monomers per arm"] + 1

    for p in range(0 + pg, pt["number of polymers"] + pg):
        poly_offset[p] = mg
        poly_N[p] = N
        rx[mg] = random.uniform(0, lx)
        ry[mg] = random.uniform(0, ly)
        rz[mg] = random.uniform(0, lz)

        if pt["type of polymer"] == "linear":
            for m in range(1 + mg, N + mg):
                bl[m, 0] = m - 1
                sig = bond_length[bead_type[m], bead_type[m - 1]] / np.sqrt(3)
                rx[m] = rx[m - 1] + random.gauss(0, sig)
                ry[m] = ry[m - 1] + random.gauss(0, sig)
                rz[m] = rz[m - 1] + random.gauss(0, sig)

        if pt["type of polymer"] == "star":
            for a in range(0, pt["number of arms"]):
                for m0 in range(0, pt["number of monomers per arm"]):
                    m = 1 + a * pt["number of monomers per arm"] + m0 + mg
                    bl[m, 0] = m - 1
                    sig = bond_length[bead_type[m], bead_type[m - 1]] / np.sqrt(3)
                    rx[m] = rx[m - 1] + random.gauss(0, sig)
                    ry[m] = ry[m - 1] + random.gauss(0, sig)
                    rz[m] = rz[m - 1] + random.gauss(0, sig)

        mg += N
        pg += 1


print("{} types of polymers and {} types of monomers.".format(n_poly_types, n_bead_types))
print(poly_list)
print("Creating total number of {} polymers, {} monomers and {} bonds.".format(n_polys, n_beads, np.sum(bl_len)))

px = np.zeros(n_beads, dtype=np.double)
py = np.zeros(n_beads, dtype=np.double)
pz = np.zeros(n_beads, dtype=np.double)
e = np.zeros(n_beads, dtype=np.double)

for m in range(n_beads):
    t = bead_type[m]
    sig = np.sqrt(1.0 / m_inv[t])
    px[m] = 0#random.gauss(0, sig)
    py[m] = 0#random.gauss(0, sig)
    pz[m] = 0#random.gauss(0, sig)
    e[m] = c[t] + 5. / 2.

px -= np.mean(px) + px_bias
py -= np.mean(py) + px_bias
pz -= np.mean(pz) + px_bias

f = h5py.File("{}_conf_t0.h5".format(arguments.name), 'w')
f.create_dataset("rx", dtype=np.double, data = rx)
f.create_dataset("ry", dtype=np.double, data = ry)
f.create_dataset("rz", dtype=np.double, data = rz)
f.create_dataset("px", dtype=np.double, data = px)
f.create_dataset("py", dtype=np.double, data = py)
f.create_dataset("pz", dtype=np.double, data = pz)
f.create_dataset("e", dtype=np.double, data = e)
f.create_dataset("m_inv", dtype=np.double, data = m_inv)
f.create_dataset("c", dtype=np.double, data = c)
f.create_dataset("bead_type", dtype=np.intc, data = bead_type)
f.create_dataset("bl", dtype=np.intc, data = bl)
f.create_dataset("bl_len", dtype=np.intc, data = bl_len)
f.create_dataset("poly_offset", dtype=np.intc, data = poly_offset)
f.create_dataset("poly_N", dtype=np.intc, data = poly_N)
f.create_dataset("kappa_bonded", dtype=np.double, data = kappa_bonded)
f.create_dataset("kappa_non_bonded", dtype=np.double, data = kappa_non_bonded)
f.create_dataset("f_bonded", dtype=np.double, data = 3. / np.multiply(bond_length, bond_length))
f.create_dataset("f_non_bonded", dtype=np.double, data = f_non_bonded)
f.create_dataset("gamma", dtype=np.double, data = gamma)
d = f.create_dataset("parameters", dtype = np.intc)
d.attrs['lx'] = np.intc(lx)
d.attrs['ly'] = np.intc(ly)
d.attrs['lz'] = np.intc(lz)
d.attrs['dt'] = np.double(dt)
d.attrs['info_dt'] = np.double(info_dt)
d.attrs['conf_save_dt_double'] = np.double(conf_save_dt_double)
d.attrs['ana_dt_double'] = np.double(ana_dt_double)
d.attrs['walltime'] = np.intc(walltime)
d.attrs['time'] = np.double(0.0)
d.attrs['n_beads'] = np.intc(n_beads)
d.attrs['n_polys'] = np.intc(n_polys)
d.attrs['bl_len_max'] = np.intc(bl_len_max)
d.attrs['n_bead_types'] = np.intc(n_bead_types)
d.attrs['n_poly_types'] = np.intc(n_poly_types)
d.attrs['n_cells'] = np.intc(n_cells)
# d.attrs['seed_master'] = np.uintc(seed_master)
f.close()

f = h5py.File("{}_ana.h5".format(arguments.name), 'w')
f.create_dataset("time", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("T_kin", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("T_int", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("e_bl", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("e_nl", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("e_kin", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("e_int", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("e_tot", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("e_delta", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("px_tot", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("py_tot", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("pz_tot", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("acc", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("acc_bl", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("acc_nl", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("n_mc", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("n_mc_bl", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("n_mc_nl", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("phi", (0, n_bead_types * n_cells), dtype=np.intc, chunks=True, maxshape=(None,n_bead_types * n_cells))
f.create_dataset("bead_msd", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("bead_msd_x", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("bead_msd_y", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("bead_msd_z", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("poly_msd", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("poly_msd_x", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("poly_msd_y", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("poly_msd_z", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("re2", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("re2_x", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("re2_y", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("re2_z", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("cl_mean", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("cl_std", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("nl_mean", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
f.create_dataset("nl_std", (0, 1), dtype=np.double, chunks=True, maxshape=(None,1))
d = f.create_dataset("parameters", dtype = np.intc)
d.attrs['lx'] = np.intc(lx)
d.attrs['ly'] = np.intc(ly)
d.attrs['lz'] = np.intc(lz)
d.attrs['dt'] = np.double(dt)
d.attrs['n_beads'] = np.intc(n_beads)
d.attrs['n_polys'] = np.intc(n_polys)
d.attrs['bl_len_max'] = np.intc(bl_len_max)
d.attrs['n_bead_types'] = np.intc(n_bead_types)
d.attrs['n_poly_types'] = np.intc(n_poly_types)
d.attrs['n_cells'] = np.intc(n_cells)
# d.attrs['seed_master'] = np.uintc(seed_master)
f.close()
