#!/usr/bin/env python

import numpy as np
import sys
import h5py
import matplotlib
import matplotlib.pyplot as plt
import glob
import scipy.stats as stats
import os

e = np.array([], dtype=np.double)
p = np.array([], dtype=np.double)

files = glob.glob("test_conf*.h5")
files.sort(key=os.path.getmtime)
file = h5py.File('test_conf_t0.h5', 'r')
c = file['/c'][0]
print('c = {}'.format(c))

for fname in files:
    #print(fname)
    file = h5py.File(fname, 'r')
    e_part = file['/e'][()]
    px = file['/px'][()]
    py = file['/py'][()]
    pz = file['/pz'][()]
    e = np.append(e, e_part)
    p = np.append(p, px)
    p = np.append(p, py)
    p = np.append(p, pz)

e = e[e.size//2:]
p = p[p.size//2:]
T0 = c / (c + 5. / 2.)

plt.subplot(1, 2, 1)
plt.hist(p, density=True, bins=100)
max = np.amax(p)
min = np.amin(p)
x = np.arange(min, max, (max - min) / 1000.)
y = stats.norm.pdf(x, scale = np.sqrt(T0))
plt.xlabel(r'$p$')
plt.ylabel(r'$\rho(p)$')
plt.plot(x, y, label = r'$\sim e^{-\frac{p^2}{2mk_\mathrm{B} T_0}}$')
plt.legend()

plt.subplot(1, 2, 2)

plt.hist(e, density=True, bins=100)
max = np.amax(e)
min = np.amin(e)
x = np.arange(min, max, (max - min) / 1000.)
y = stats.gamma.pdf(x, a = c + 1, scale = T0)
plt.xlabel(r'$\epsilon$')
plt.ylabel(r'$\rho(\epsilon)$')
plt.plot(x, y, label = r'$\sim\epsilon^\frac{c_0}{k_\mathrm{B}}e^{-\frac{\epsilon}{k_\mathrm{B} T_0}}$')
plt.legend()

plt.tight_layout()

plt.savefig('histograms.png', dpi = 300)
